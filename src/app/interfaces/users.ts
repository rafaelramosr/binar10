export interface Users {
  id: number,
  name: string,
  surname: string,
  document: number,
  email: string,
  url: string,
}
