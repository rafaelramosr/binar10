import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SessionStorageService {
  constructor() {}

  encryptedText(data: string): Uint8Array {
    const enc = new TextEncoder();
    const encodedData = enc.encode(data);
    return encodedData;
  }

  decryptedText(data: Uint8Array): string {
    const dec = new TextDecoder();
    const decodedData = dec.decode(data);
    return decodedData;
  }

  getStorage(key: string): Object | null {
    const dataStorage = sessionStorage.getItem(key);
    if (!dataStorage) return null;
    const parseData = new Uint8Array(JSON.parse(dataStorage));
    const decryptedData = this.decryptedText(parseData);
    return JSON.parse(decryptedData);
  }

  setStorage(key: string, value: any): void {
    const encryptedData = this.encryptedText(JSON.stringify(value));
    sessionStorage.setItem(key, `[${encryptedData.toString()}]`);
  }

  clearStorage(): void {
    sessionStorage.clear();
  }
}
