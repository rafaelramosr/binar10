import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  public getById(path: string, id: number): Observable<any> {
    const url = `${environment.api}${path}/${id}`;
    const options = {
      headers: new HttpHeaders().set('Token', `Token ${environment.token}`),
    };

    return this.http.get<any>(url, options);
  }

  public get(path: string): Observable<any> {
    const url = `${environment.api}${path}`;
    const options = {
      headers: new HttpHeaders().set('Token', `Token ${environment.token}`),
    };

    return this.http.get<any>(url, options);
  }

  public post(path: string, body: any): Observable<any> {
    const url = `${environment.api}${path}`;

    return this.http.post(url, body);
  }
}
