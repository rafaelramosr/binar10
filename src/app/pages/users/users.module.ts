import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UsersComponent } from './users.component';
import {
  FileImgComponent,
  ModalContentComponent,
  NavbarComponent,
  UserAvatarComponent,
  UserFormComponent,
  UserCardComponent
} from '../../components';
import { UserRoutingModule } from './user-routing.module';

@NgModule({
  declarations: [
    FileImgComponent,
    ModalContentComponent,
    NavbarComponent,
    UserAvatarComponent,
    UserFormComponent,
    UserCardComponent,
    UsersComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UserRoutingModule,
  ]
})
export class UsersModule { }
