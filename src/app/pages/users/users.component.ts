import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { RestService } from '../../services/rest/rest.service';
import { Users } from '../../interfaces/users';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.sass'],
})
export class UsersComponent implements OnInit {
  modalShow = false;
  users: Array<Users> = [];

  constructor(
    private route: ActivatedRoute,
    private restService: RestService
  ) {}

  ngOnInit(): void {
    this.restService.get('/usuario').subscribe((res) => {
      this.users = this.parseUserData(res);
    });

    const action = this.route.snapshot.params.action;
    this.modalShow = ['add', 'edit'].includes(action);
  }

  parseUserData(userData: Array<any>) {
    return userData.map((user) => {
      const { id, nombre, apellido, cedula, email, url } = user;
      return {
        id,
        name: nombre,
        surname: apellido,
        document: cedula,
        email,
        url,
      };
    });
  }
}
