import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-file-img',
  templateUrl: './file-img.component.html',
  styleUrls: ['./file-img.component.sass'],
})
export class FileImgComponent implements OnInit {
  currentImage: string = 'assets/images/file-background.jpg';
  @Output() file = new EventEmitter();

  ngOnInit(): void {}

  convertFile(filePath: any) {
    return new Promise((resolve, reject) => {
      try {
        const reader = new FileReader();
        reader.readAsDataURL(filePath);
        reader.onload = () => {
          resolve({
            base: reader.result,
          });
        };
        reader.onerror = (error) => reject(error);
      } catch (error) {
        reject(error);
      }
    });
  }

  setFileInput(event: any): void {
    const filePath = event.target.files[0];
    this.convertFile(filePath).then(
      (image: any) => (this.currentImage = image.base)
    );
    this.file.emit([filePath]);
  }
}
