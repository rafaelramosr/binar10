import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileImgComponent } from './file-img.component';

describe('FileImgComponent', () => {
  let component: FileImgComponent;
  let fixture: ComponentFixture<FileImgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FileImgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
