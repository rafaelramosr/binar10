import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { RestService } from '../../services/rest/rest.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.sass'],
})
export class UserFormComponent implements OnInit {
  currentAction: string = '';
  userForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private restService: RestService
  ) {
    this.userForm = this.formBuilder.group({
      document: [''],
      entity: [''],
      email: [''],
      name: [''],
      password: [''],
      password2: [''],
      surname: [''],
      username: [''],
    });
  }

  ngOnInit(): void {
    const params = this.route.snapshot.params;
    if (params.id) {
      this.restService.getById('/usuario', params.id).subscribe(res => {
        const userData = this.parseEditData(res);
        this.userForm.patchValue(userData);
      })
    }
    this.currentAction = params.action;
  }

  parseEditData(userData: any) {
    return {
      name: userData.nombre,
      surname: userData.apellido,
      entity: userData.entidad,
      password: userData.email,
    };
  }

  getFile(event: any) {
    console.log(event);
  }

  send() {
    console.log(this.userForm.value);
  }
}
