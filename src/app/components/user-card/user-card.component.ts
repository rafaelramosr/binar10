import { Component, OnInit, Input } from '@angular/core';

import { Users } from "../../interfaces/users";

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.sass']
})
export class UserCardComponent implements OnInit {

  @Input() user: Users = {} as Users;

  constructor() { }

  ngOnInit(): void {
  }

}
