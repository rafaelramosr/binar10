import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from '../../services/session-storage/session-storage.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass'],
})
export class NavbarComponent {
  constructor(
    private sessionStorageService: SessionStorageService,
    private router: Router
  ) {}

  logout() {
    this.sessionStorageService.clearStorage();
    this.router.navigate(['/login']);
  }
}
