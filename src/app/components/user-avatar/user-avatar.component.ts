import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-avatar',
  templateUrl: './user-avatar.component.html',
  styleUrls: ['./user-avatar.component.sass']
})
export class UserAvatarComponent implements OnInit {

  @Input() image: string = '';
  @Input() altText: string = '';
  avatar: string = '';

  constructor() { }

  ngOnInit(): void {
    if (!this.image) {
      this.avatar = this.avatarInitials(this.altText);
    }
  }

  avatarInitials(name: string): string {
    const arrName = name.split(' ');
    const firstInitial = arrName[0][0];

    if (arrName.length === 1) return firstInitial;

    const secondIndex = arrName.length - Math.ceil(arrName.length / Math.E);
    const secondInitial = arrName[secondIndex][0];

    return `${firstInitial}${secondInitial}`;
  }

}
