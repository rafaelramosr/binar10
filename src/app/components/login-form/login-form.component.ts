import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { RestService } from '../../services/rest/rest.service';
import { SessionStorageService } from "../../services/session-storage/session-storage.service";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.sass'],
})
export class LoginFormComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private restService: RestService,
    private sessionStorageService: SessionStorageService,
    private router: Router
  ) {
    this.loginForm = this.formBuilder.group({
      email: [''],
      password: [''],
    });
  }

  ngOnInit(): void {
    const isLogin = this.sessionStorageService.getStorage('key');
    if (isLogin) this.router.navigate(['/users']);
  }

  send() {
    const loginData = new FormData();
    loginData.append('email', this.loginForm.value.email);
    loginData.append('password', this.loginForm.value.password);

    this.restService.post('/login/', loginData).subscribe(res => {
      this.sessionStorageService.setStorage('key', res);
      this.router.navigate(['/users']);
    })
  }
}
